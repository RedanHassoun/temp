package maroun.com.flickrapp.page.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import maroun.com.flickrapp.R;
import maroun.com.flickrapp.model.GalleryItem;

/**
 * Created by Redan on 4/16/2017.
 */
public class GalleryAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<GalleryItem> items;
    private static LayoutInflater inflater=null;

    public GalleryAdapter(Context context, ArrayList<GalleryItem> items) {
        this.context = context;
        this.items =items;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.item_gallery, null);

        Holder holder = new Holder();
        holder.imageView = (ImageView) vi.findViewById(R.id.image);

        if((items.get(position)!=null)&&(!items.get(position).equals("")))
        {
            Picasso.with(context)
                    .load(items.get(position).getUrl())
                    .placeholder(R.drawable.ic_loading_icon_with_fade)
                    .error(R.mipmap.ic_launcher)
                    .fit()
                    .centerCrop()
                    .into(holder.imageView);
        }

        return vi;
    }

    public void clear()
    {
        items = new ArrayList<>();
    }

    public void addAll(ArrayList<GalleryItem> newUrls)
    {
        this.items.addAll(newUrls);
    }

    private class Holder
    {
        ImageView imageView;
    }
}
