package maroun.com.flickrapp.page.gallery;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import maroun.com.flickrapp.R;
import maroun.com.flickrapp.model.GalleryItem;
import maroun.com.flickrapp.util.UrlManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Redan on 4/16/2017.
 */
public class FragmentGallery extends Fragment
{
    private View view;
    private GalleryAdapter adapter;
    private ArrayList<GalleryItem> urls = new ArrayList<>();
    private SearchView searchView;
    private static final int ITEMS_PER_PAGE = 20;
    private static final String TAG = "fragment_gallery";
    private boolean mHasMore = true;
    private boolean isLoading = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,
                                                     @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_gallery,container,false);

        searchView = (SearchView) view.findViewById(R.id.search_view);
        adapter = new GalleryAdapter(getContext(),urls);
        GridView gridView = (GridView) view.findViewById(R.id.gridview);
        gridView.setAdapter(adapter);

        initSearchView();

        return view;
    }

    public void refresh()
    {
        adapter.clear();
        if(!isLoading)
            startLoading();
    }

    private void startLoading()
    {
        String queryString =  PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getString(UrlManager.PREF_SEARCH_QUERY, null);

        final int page = adapter.getCount() / ITEMS_PER_PAGE;
        String url = UrlManager.getItemUrl(queryString, page);
        Log.d(TAG,"URL: "+url);

        new ImageLoaderTask(page).execute(url);
    }

    private class ImageLoaderTask extends AsyncTask<String,Void,JSONObject>
    {
        private int page;
        public ImageLoaderTask(int page)
        {
            this.page = page;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(params[0])
                        .build();

                okhttp3.Response response = client.newCall(request).execute();
                Log.d(TAG,"RESPONSE : "+response.body().toString());
                jsonObject = new JSONObject(response.body().string());

            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                return jsonObject;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject)
        {
            super.onPostExecute(jsonObject);

            if(jsonObject != null){
                ArrayList<GalleryItem> result = new ArrayList<>();
                try {
                    JSONObject photos = jsonObject.getJSONObject("photos");
                    if (photos.getInt("pages") == page) {
                        mHasMore = false;
                    }
                    JSONArray photoArr = photos.getJSONArray("photo");
                    for (int i = 0; i < photoArr.length(); i++) {
                        JSONObject itemObj = photoArr.getJSONObject(i);
                        GalleryItem item = new GalleryItem(
                                itemObj.getString("id"),
                                itemObj.getString("secret"),
                                itemObj.getString("server"),
                                itemObj.getString("farm")
                        );
                        result.add(item);
                    }
                } catch (JSONException e) {

                }
                adapter.addAll(result);
                adapter.notifyDataSetChanged();
                isLoading = false;
            }else {
                // TODO: ERROR case
            }
        }
    }

    private void initSearchView()
    {
        SearchManager searchManager =
                                (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo =
                                    searchManager.getSearchableInfo(getActivity().getComponentName());

        SearchView sv = (SearchView)view.findViewById(R.id.search_view);
        sv.setSearchableInfo(searchableInfo);
    }

}

