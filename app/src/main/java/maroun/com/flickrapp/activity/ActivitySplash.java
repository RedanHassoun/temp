package maroun.com.flickrapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import maroun.com.flickrapp.R;
import maroun.com.flickrapp.page.gallery.ActivityGallery;

/**
 * Created by Redan on 4/16/2017.
 */
public class ActivitySplash extends AppCompatActivity
{
    private static final int KEEP_TIME = 2000;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(getActionBar() != null)
            getActionBar().hide();

        initView();
    }

    private void initView()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(ActivitySplash.this, ActivityGallery.class);
                startActivity(intent);
                finish();
            }
        }, KEEP_TIME);
    }
}
